今天制作个拖拽树组件。

## Tree

首先，先要完成树组件制作。

树组件通常是直接传个树的列表，然后根据这个列表进行显示。

这里硬渲染会有问题，一般做法是递归时使用数组拍平，并且给数据增加一些额外标识。

可以看个示例：

```js
const source = [
	{
	  value: "北京分行",
	  children: [
		{
		  value: "朝阳支行办事处",
		  children: [
			{   value: "朝阳支行办事处-1" },
			{   value: "朝阳支行办事处-2" }
		  ]
		},
		{   value: "海淀支行办事处" },
		{  value: "石景山支行办事处" }
	  ]
	},
	{
	  value: "天津分行",
	  children: [
		{   value: "和平支行办事处" },
		{  value: "河东支行办事处" },
		{ value: "南开支行办事处" }
	  ]
	}
];

const flatten = function(
	list,
	childKey = "children",
	level = 1,
	parent = null,
	defaultExpand = true
  ) {
	let arr = [];
	list.forEach(item => {
	  item.level = level;
	  if (item.expand === undefined) {
		item.expand = defaultExpand;
	  }
	  if (item.visible === undefined) {
		item.visible = true;
	  }
	  if (!parent.visible || !parent.expand) {
		item.visible = false;
	  }
	  item.parent = parent;
	  arr.push(item);
	  if (item[childKey]) {
		arr.push(
		  ...flatten(
			item[childKey],
			childKey,
			level + 1,
			item,
			defaultExpand
		  )
		);
	  }
	});
	return arr;
};

console.log(flatten(source,'children',1,{
	level: 0,
	visible: true,
	expand: true,
	children:source
}))
```

这样通过一个递归把数组拍平了，下面显示就可以只遍历visible进行显示即可。缩进就结合深度，扩展角标结合expend。同时，这个拍平还可以结合虚拟列表，实现无限滚动效果。

## 基本框架

首先制作基本框架，对于children字段如果不指定会有问题，所以我们限定死必须children字段，同时，增加key，这个key与层级无关，就是react渲染用的（这里为了方便生成Key，最好需要用户传key，不提供key可能产生bug）。

然后我们要制作可以展开关闭的情况，也就是每个div绑定事件，这个事件需要传递他自己，然后我们改变它的属性 ， 由于react跟vue不一样没做代理，所以这个采用在原数据上更新后手动刷新方法，而非直接返回个新对象。

```tsx
const source = [
	{
		value: "北京分行",
		children: [
			{
				value: "朝阳支行办事处",
				children: [
					{ value: "朝阳支行办事处-1" },
					{ value: "朝阳支行办事处-2" },
				],
			},
			{ value: "海淀支行办事处" },
			{ value: "石景山支行办事处" },
		],
	},
	{
		value: "天津分行",
		children: [
			{ value: "和平支行办事处" },
			{ value: "河东支行办事处" },
			{ value: "南开支行办事处" },
		],
	},
];

const flatten = function(
	list: Array<itemProps>,
	level = 1,
	parent: itemProps,
	defaultExpand = true
): itemPropsRequired[] {
	let arr: itemPropsRequired[] = []; //收集所有子项
	list.forEach((item) => {
		item.level = level;
		if (item.expand === undefined) {
			item.expand = defaultExpand;
		}
		if (item.visible === undefined) {
			item.visible = true;
		}
		if (!parent.visible || !parent.expand) {
			item.visible = false;
		}
		if (item.key === undefined) {
			item.key = item.value + Math.random();
		}

		item.parent = parent;
		arr.push(item as itemPropsRequired);
		if (item["children"]) {
			arr.push(
				...flatten(item["children"], level + 1, item, defaultExpand)
			);
		}
	});
	return arr;
};

const root = {
	level: 0,
	visible: true,
	expand: true,
	children: source,
	value: "root",
};

interface itemProps {
	value: string;
	level?: number;
	expand?: boolean;
	visible?: boolean;
	parent?: itemProps;
	children?: Array<itemProps>;
	key?: string;
}

interface itemPropsRequired
	extends Omit<Required<itemProps>, "children" | "parent"> {
	children?: itemPropsRequired[];
	parent: itemPropsRequired;
}

type TreeProps = {};

const changeVisible = (item: itemPropsRequired, callback: Function) => {
	//给点击的children设置visible
	if (item.children) {
		//避免children有显示不一行为
		let visible: boolean;
		const depth = (item: itemPropsRequired[]) => {
			item.forEach((v) => {
				if (visible === undefined) {
					visible = !v.visible;
				}
				v.visible = visible;
				if (v.children) {
					//把孩子全部改掉
					depth(v.children);
				}
			});
		};
		depth(item.children);
		callback(); //改完后更新页面
	}
};

export function Tree(props: TreeProps) {
	const data = flatten(source, 1, root);
	const forceUpdate = useState(0)[1];
	return (
		<div>
			{data
				.filter((v) => v.visible === true)
				.map((g) => (
					<div
						onClick={() =>
							changeVisible(g, () => {
								forceUpdate((state) => state + 1);
							})
						}
						key={g.key}
						style={{
							paddingLeft: `${10 * g.level}px`,
							cursor: "pointer",
						}}
					>
						{g.value}
					</div>
				))}
		</div>
	);
}
```

这样就是一个基本树组件了，还可以点击开闭。并且性能也不错，每次不需要递归整个数据，只要递归对应的孩子即可。当然如果有兴趣的同学，可以结合react的思路将递归进行拆解，真正的完美不阻塞浏览器，这样支持更庞大的数据量。这个不是本节课内容，具体可以参考张仁阳老师的react源码课。

下面制作拖拽。

拖拽需要在元素上加个draggable属性，这样按住鼠标左键，鼠标下面会有该元素的影子跟着鼠标。

被拖动的元素会触发dragstart一次，ondrag n次，拖动结束会触发dragend。

目标元素：

拖动它还有拖到哪，就是目标元素，目标元素需要监听事件才能开启，事件有：

dragenter,dragover,dragleave,drop

dragover就是拖动元素到目标元素上方，目标元素不断触发。

dragenter拖进目标元素里触发一次。

dragleave拖出目标元素，触发一次。

drop，松开左键，触发一次。**注意！这里有个坑，必须把dragover事件 preventDefault才能触发drop**

另外，拖拽了我们需要通信啊，不然数据怎么改不知道。

事件对象里有个dataTransfer可以携带数据。不过只能带字符串或者url。

mdn :https://developer.mozilla.org/zh-CN/docs/Web/API/DataTransfer

```tsx
export function Tree(props: TreeProps) {
	const data = flatten(source, 1, root);
	const forceUpdate = useState(0)[1];
	return (
		<div>
			{data
				.filter((v) => v.visible === true)
				.map((g) => (
					<div
						draggable
						onClick={() =>
							changeVisible(g, () => {
								forceUpdate((state) => state + 1);
							})
						}
						key={g.key}
						style={{
							paddingLeft: `${10 * g.level}px`,
							cursor: "pointer",
						}}
						onDragStart={(e) => {
							e.dataTransfer.setData("ff", "22");
						}}
						onDragOver={(e) => {
							e.preventDefault();
						}}
						onDrop={(e) => {
					console.log(e.dataTransfer.getData("ff"));
						}}
					>
						{g.value}
					</div>
				))}
		</div>
	);
}
```

可以试试，如果能顺利传递，说明就ok。

有了数据传递，下面需要确定传递啥，由于key做的唯一，可以传递key，来知道拖动的对象是谁。

目标对象收到拖拽目标的key后，就有个问题，它到底是拖到同级还是拖到下级或者上级？

这里我踩了坑，准备拖拽目标元素把孩子节点带过去，但是后来发现，这里会有个bug，如果拖拽元素拖拽到其下级元素上，就产生了bug，而且比较难搞，事实上vue-drag-tree确实有这个bug，如果这么拖，会把节点搞消失了。https://vigilant-curran-d6fec6.netlify.app/#/

这里我会对这个情况单独做个判断，后面说。

如果不满意拖动时的样子，也可以换图片。

换拖动图片也是靠dataTransfer，是上面提供的一个方法。

mdn :https://developer.mozilla.org/zh-CN/docs/Web/API/DataTransfer/setDragImage

这个方法也有个坑，就是没显示在页面上的canvas不会显示图像。。。**必须要已出现在页面里的canvas才可以**。我自己canvas画了个图，放上去不显示，必须append到文档上才显示。所以这里采用图片方式比较好。找个图片然后src等于它就可以了。

这里我网上找了个图片，用来替换掉：

```
const img = new Image();
img.src = "https://www.easyicon.net/api/resizeApi.php?id=1200841&size=32";
```

```
	onDragStart={(e) => {
							e.dataTransfer.setData("ff", "22");
							e.dataTransfer.setDragImage(img, 29, 29);
						}}
```

效果还可以。

下面就要实现拖动完成修改列表了

我们需要传递拖动对象的key，然后在拖动目标上进行对应逻辑。

由于这个插入逻辑比较复杂，所以梳理一下:

有4种情况，插上级，插同级，插下级，不变。

插上级需要用户拖到比padding前触发，插上级时，需要查找拖拽对象的孩子，如果目标对象在拖拽对象孩子里（不管多少层），那么是无效拖拽，什么都不做。

进入插上级函数前，需进行一个判断，如果目标对象是顶级，那么不应该走这个逻辑。对象如果是它自己，也不能走这个逻辑，否则可能产生循环引用等问题。

正常执行插上级流程时，需要修改目标对象的parent的children，如果目标对象没children就添加，否则push。然后需要递归拖拽对象的children，改变所有层级。同时拖拽对象原上级的孩子需要去掉它。

还有新孩子问题，插入上级后，目标节点父亲的孩子从目标节点往后，应该变成新下级，需要纳入children。但是上方节点插到下方节点和下方节点插到上方节点不相同，需要分情况讨论。

此外，还有个顺序问题，如果我们直接把孩子加到末尾，那么拖拽元素就跑末尾去了，所以还需要确定父级节点的位置。

最后，还有个扁平顺序问题，由于用的是不变数据源，并且是在其对象上直接修改，所以无法生成第二次，还必须修改扁平顺序。

扁平顺序如果是带着孩子插入，那么就是在目标节点后方开始插，如果没带孩子，那么找到位置插入即可。同理，上方节点插到上方和上方插到下方不一样。

这个只是插上级的，还有插同级插下级类比插上级情况。

上面这个逻辑过于复杂，但是效率还算比较高，有兴趣同学可以试试，我写了半天，结果还是有逻辑疏漏产生的bug。。于是采用另一种方法：

上面那方法复杂地方就在于还要**正确**修改拍平的顺序，那么我们可以直接获取完整的树，进行修改，然后复用第一个方法进行自动拍平，这样就可以尽可能减少bug，只要关心树结构是否正确即可，与拍平相关逻辑不用考虑。这样效率会比上面那种低一点，因为需要整套全部递归一遍，但是好写多了。后面可以暴露个开关，是否开启拖拽，数据量大就不用拖拽。

复用方式就是可以拿到更新后的root的children作为source，执行flatten。

对于复用更新，我们最好新建个变量而不是用前面的forceUpdate，因为收起展开并不需要拍平，直接就可以用已经拍平好的，只有整颗树的变化才需要执行拍平函数：

```typescript
	const root = useMemo(() => {
		return {
			level: 0,
			visible: true,
			expand: true,
			children: source,
			value: "root",
		};
	}, [source]);
	const [dragUpdate, setDragUpdate] = useState(0);
	const data = useMemo(() => {
		return flatten(root.children, 1, root);
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [root, dragUpdate]);
```

手动更新就可能会导致eslint报错，把它注掉就可以了。

渲染部分：

```tsx
export function Tree(props: TreeProps) {
	const root = useMemo(() => {
		return {
			level: 0,
			visible: true,
			expand: true,
			children: source,
			value: "root",
		};
	}, [source]);
	const [dragUpdate, setDragUpdate] = useState(0);
	const data = useMemo(() => {
		return flatten(root.children, 1, root);
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [root, dragUpdate]);
	const [start, setStart] = useState(0);
	const forceUpdate = useState(0)[1];
	const ref = useRef<HTMLDivElement>(null);
	useEffect(() => {
		if (ref.current) {
			setStart(ref.current.getBoundingClientRect().left); //为了找到起始
		}
	}, []);
	const callback = () => {
		forceUpdate((state) => state + 1);
	};
	const dragCallback = () => {
		setDragUpdate((state) => state + 1);
	};
	return (
		<div ref={ref}>
			{data
				.filter((v) => v.visible === true)
				.map((g) => {
					return (
						<div
							draggable
							onClick={() => changeVisible(g, callback)}
							key={g.key}
							style={{
								paddingLeft: `${10 * g.level}px`,
								cursor: "pointer",
							}}
							onDragStart={(e) => {
								e.dataTransfer.setData("atomkey", `${g.key}`);
								e.dataTransfer.setDragImage(img, 29, 29);
							}}
							onDragOver={(e) => {
								e.preventDefault();
							}}
							onDrop={(e) => {
								const key = e.dataTransfer.getData("atomkey");
								const left = e.clientX;
								const diff = left - start; //离顶部差值
								const res = switchInsert(diff, g);
								switch (res) {
									case 1:
										insertTop(key, g, data, dragCallback);
										break;
									case 2:
										insertMiddle(
											key,
											g,
											data,
											dragCallback
										);
										break;
									case 3:
										insertLower(key, g, data, dragCallback);
										break;
									default:
										break;
								}
							}}
						>
							{g.value}
						</div>
					);
				})}
		</div>
	);
}
```

函数部分：

```typescript
interface itemProps {
	value: string;
	level?: number;
	expand?: boolean;
	visible?: boolean;
	parent?: itemProps;
	children?: Array<itemProps>;
	key?: string;
}

interface itemPropsRequired
	extends Omit<Required<itemProps>, "children" | "parent"> {
	children?: itemPropsRequired[];
	parent: itemPropsRequired;
}
const checkParent = function(g: itemPropsRequired) {
	return g.level === 1;
};

const levelSpace = 20; //同级生效间距
///1插上级 2插同级 3插下级 0不插
const switchInsert = function(diff: number, g: itemPropsRequired) {
	if (!isNaN(diff)) {
		const origin = g.level * 10; //目标原本padding
		if (diff < origin) {
			//移动到padding前全部算上级
			if (checkParent(g)) {
				//排除最顶级
				return 2;
			} else {
				return 1;
			}
		} else if (diff < origin + levelSpace) {
			return 2;
		} else {
			return 3;
		}
	} else {
		return 0;
	}
};

const findOrigin = function(key: string, data: itemPropsRequired[]) {
	const res = data.filter((v) => v.key === key);
	if (res.length > 0) {
		return res[0];
	} else {
		return null;
	}
};
const getParent = function(g: itemPropsRequired) {
	if (g.parent && g.parent.parent) {
		return g.parent.parent;
	} else {
		return g.parent;
	}
};

const judgeChildren = function(
	origin: itemPropsRequired,
	target: itemPropsRequired
) {
	let sign = true; //如果有孩子就是false
	const fn = (child: itemPropsRequired) => {
		if (child.children) {
			child.children.forEach((v) => {
				if (v === target) {
					sign = false;
					return;
				}
				fn(v);
			});
		}
	};
	fn(origin);
	return sign;
};
const changeOriginParent = function(origin: itemPropsRequired) {
	const parent = origin.parent;
	if (parent.children) {
		const index = parent.children.indexOf(origin);
		if (index > -1) {
			parent.children.splice(index, 1);
		}
		//下面这个方法会产生bug
		//parent.children = parent.children.filter((v) => v !== origin);
	}
};

const changeTargetParent = function(
	parent: itemPropsRequired,
	origin: itemPropsRequired,
	g: itemPropsRequired
) {
	origin.parent = parent;
	if (parent.children) {
		//判断应该插入父级节点哪里
		if (g.parent.children) {
			const index = g.parent.children.indexOf(g);
			if (index > -1) {
				parent.children.splice(index + 1, 0, origin);
			} else {
				//parent传递g会进来
				parent.children.push(origin);
			}
		} else {
			parent.children.push(origin);
		}
	} else {
		parent.children = [origin];
	}
};


const checkTargetOrigin = function(
	g: itemPropsRequired,
	origin: itemPropsRequired
) {
	return g !== origin;
};

const insertTop = function(
	key: string,
	g: itemPropsRequired,
	data: itemPropsRequired[],
	callback: Function
) {
	const origin = findOrigin(key, data);
	//origin插入target上级
	const parent = getParent(g);
	if (
		g.level !== 1 &&
		origin &&
		checkTargetOrigin(g, origin) &&
		judgeChildren(origin, g)
	) {
		//修改以前父节点
		changeOriginParent(origin);
		//修改目标父节点的父节点（与父节点同级）
		changeTargetParent(parent, origin, g);
		callback();
	}
};
const insertMiddle = function(
	key: string,
	g: itemPropsRequired,
	data: itemPropsRequired[],
	callback: Function
) {
	const origin = findOrigin(key, data);
	//origin插入target同级
	const parent = g.parent;
	if (
		g.level !== 0 &&
		origin &&
		checkTargetOrigin(g, origin) &&
		judgeChildren(origin, g)
	) {
		changeOriginParent(origin);
		changeTargetParent(parent, origin, g);
		callback();
	}
};

const insertLower = function(
	key: string,
	g: itemPropsRequired,
	data: itemPropsRequired[],
	callback: Function
) {
	const origin = findOrigin(key, data);
	const parent = g;
	if (origin && checkTargetOrigin(g, origin) && judgeChildren(origin, g)) {
		changeOriginParent(origin);
		changeTargetParent(parent, origin, g);
		callback();
	}
};
```

## 功能与美化

上面代码逻辑已经全跑通了，下面需要增加功能与美化。

对于有children可展开关闭的，我们应该增加个标识。

这里稍微注意下这个标识宽度，我们把标识加到文字左边，这样标识宽度会影响第一个字位置，下一级的padding应该要大于到第一个文字的长度才行。

所以我拿个变量记录间距：

```
const originPadding = 24; //原始间距
```

对于前面写10，并且要间距的部分进行修改。

使用styled组件替换以前的div：

```typescript
interface TreeItemType {
	level: number;
}

const TreeItem = styled.div<TreeItemType>`
	padding-left: ${(props) => originPadding * props.level}px;
	padding-top: 2px;
	padding-bottom: 2px;
	display: flex;
	align-items: center;
	position: relative;
	overflow: hidden;
`;

const TreeIcon = styled.span<{ g: itemPropsRequired }>`
	& > svg {
		transition: linear 0.2s;
		height: 10px;
		margin-bottom: 5px;
		${(props) => {
			if (props.g.children && props.g.children.length !== 0) {
				if (props.g.children[0] && props.g.children[0]["visible"]) {
					return "display:inline-block;transform: rotate(-90deg);";
				} else {
					return "display:inline-block;";
				}
			} else {
				return "opacity:0";
			}
		}};
	}
`;
```

渲染部分：

```tsx
return (
		<div ref={ref}>
			{data
				.filter((v) => v.visible === true)
				.map((g) => {
					return (
						<TreeItem
							level={g.level}
							draggable
							onClick={() => changeVisible(g, callback)}
							key={g.key}
							onDragStart={(e) => {
								e.dataTransfer.setData("atomkey", `${g.key}`);
								e.dataTransfer.setDragImage(img, 29, 29);
							}}
							onDragOver={(e) => {
								e.preventDefault();
							}}
							onDrop={(e) => {
								const key = e.dataTransfer.getData("atomkey");
								const left = e.clientX;
								const diff = left - start; //离顶部差值
								const res = switchInsert(diff, g);
								switch (res) {
									case 1:
										insertTop(key, g, data, dragCallback);
										break;
									case 2:
										insertMiddle(
											key,
											g,
											data,
											dragCallback
										);
										break;
									case 3:
										insertLower(key, g, data, dragCallback);
										break;
									default:
										break;
								}
							}}
						>
							<TreeIcon g={g}>
								<Icon icon="arrowdown"></Icon>
							</TreeIcon>
							<span>{g.value}</span>
						</TreeItem>
					);
				})}
		</div>
	);
```

这样效果就好看多了，下面对拖拽进行优化。

因为我们拖拽有3种状态，拖到同级上级或者下级，那么最好要有个标识在拖动中可以动态显示是否满足上述状态。

这样，就需要给个状态判定拖拽中，然后记录dragover产生的x值，改变样式。

这里我使用一个带颜色的长条来标识拖拽中的长度。

由于dragOver触发过于频繁，这里我们使用节流，而不是防抖（因为悬停仍然会一直触发）。

我们简单写个节流：

```typescript
export function throttle(fn: Function, delay: number = 300) {
	let flag = true;
	return function(...args: any) {
		if (flag) {
			flag = false;
			fn(...args);
			setTimeout(() => {
				flag = true;
			}, delay);
		}
	};
}
```

滑动时候只要setState就可以了:

```typescript
const [dragOver, setDragOver] = useState<DragControlData>({
		drag: false,
		x: 0,
		itemkey: "",
	});

	const dragHandler = (
		clientX: number,
		itemkey: string,
		g: itemPropsRequired
	) => {
		const diff = clientX - start;
		const x = switchInsert(diff, g);
		setDragOver({
			drag: true,
			x,
			itemkey,
		});
	};

```

````tsx
	onDragOver={(e) => {
								e.preventDefault();
								throttle(dragHandler)(e.clientX, g.key, g);
							}}
````

使用styled制作拖动标识：

```typescript
interface DragControlData {
	drag: boolean;
	x: number;
	itemkey: string;
}
type TreeGragType = { gkey: string } & DragControlData;

const TreeGrag = styled.div<TreeGragType>`
	position: absolute;
	width: 100%;
	height: 90%;
	${(props) => {
		switch (props.x) {
			case 1:
				return `margin-left:${-levelSpace}px ;`;
			case 2:
				return "";
			case 3:
				return `margin-left:${levelSpace}px  ;`;
			default:
				return "";
		}
	}};
	${(props) => {
		if (props.itemkey === props.gkey) {
			return "background: #00000030;";
		}
	}}
`;
```

渲染部分：

```
	{dragOver.drag && (
								<TreeGrag
									gkey={g.key}
									drag={dragOver.drag}
									x={dragOver.x}
									itemkey={dragOver.itemkey}
								></TreeGrag>
							)}
```

对于鼠标拖拽出去，可能会有显示问题，所以给window监听事件：

```
	useEffect(() => {
		const handler = () => {
			setDragOver((prev) => ({ ...prev, drag: false }));
		};

		window.addEventListener("dragend", handler);
		return () => {
			window.removeEventListener("dragend", handler);
		};
	}, []);
```

这样体验就很好了。另外，在拖拽时，最好高亮下原拖拽元素，这里我加个变量解决：

```
	const [highlight, setHighlight] = useState({
		drag: true,
		itemkey: "",
	});
```

监听dropend中关闭它，传递给treeitem组件：

```typescript
useEffect(() => {
		const handler = () => {
			setDragOver((prev) => ({ ...prev, drag: false }));
			setHighlight({
				drag: false,
				itemkey: "",
			});
		};
		window.addEventListener("dragend", handler);
		return () => {
			window.removeEventListener("dragend", handler);
		};
	}, []);
```

```
	<TreeItem
							itemkey={g.key}
							highlight={highlight}
							level={g.level}
							draggable
							onClick={() => changeVisible(g, callback)}
							key={g.key}
							onDragStart={(e) => {
								e.dataTransfer.setData("atomkey", `${g.key}`);
								e.dataTransfer.setDragImage(img, 29, 29);
								setHighlight({
									drag: true,
									itemkey: g.key,
								});
							}}
```

treeItem拿到属性后做个判定即可：

```typescript
interface DragHighlight {
	drag: boolean;
	itemkey: string;
}

interface TreeItemType {
	level: number;
	itemkey: string;
	highlight: DragHighlight;
}

const TreeItem = styled.div<TreeItemType>`
	padding-left: ${(props) => originPadding * props.level}px;
	padding-top: 2px;
	padding-bottom: 2px;
	display: flex;
	align-items: center;
	position: relative;
	overflow: hidden;
	${(props) => {
		if (props.highlight.drag && props.highlight.itemkey === props.itemkey) {
			return "border: 1px dashed #53c94fa8;";
		} else {
			return "";
		}
	}}
`;
```

这样美化工作基本完成，下面暴露接口，暴露接口部分可以自由发挥，我这里就简单配置下：

```
export type TreeProps = {
	/** 数据源*/
	source: itemProps[];
	/** 是否可以拖拽 */
	drag?: boolean;
	/** 高亮边框颜色 */
	borderColor?: string;
	/** 拖拽提示色 */
	backColor?: string;
	/**外层样式*/
	style?: CSSProperties;
	/**外层类名*/
	classname?: string;
};
Tree.defaultProps = {
	source: [],
	drag: true,
	borderColor: "#53c94fa8",
	backColor: "#00000030",
};
```

## 编写story

老样子，暴露几个可以配置的就ok了。

```tsx
import React from "react";
import { Tree } from "./index";
import { withKnobs, boolean, color } from "@storybook/addon-knobs";

export default {
	title: "Tree",
	component: Tree,
	decorators: [withKnobs],
};

const source = [
	{
		value: "北京分行",
		children: [
			{
				value: "朝阳支行办事处",
				children: [
					{ value: "朝阳支行办事处-1" },
					{ value: "朝阳支行办事处-2" },
				],
			},
			{ value: "海淀支行办事处" },
			{ value: "石景山支行办事处" },
		],
	},
	{
		value: "天津分行",
		children: [
			{ value: "和平支行办事处" },
			{ value: "河东支行办事处" },
			{ value: "南开支行办事处" },
		],
	},
];

export const knobsTree = () => (
	<Tree
		backColor={color("backColor", "#00000030")}
		borderColor={color("borderColor", "#53c94fa8")}
		drag={boolean("drag", true)}
		source={source}
	></Tree>
);
```

## 今日作业

完成拖拽树组件。

